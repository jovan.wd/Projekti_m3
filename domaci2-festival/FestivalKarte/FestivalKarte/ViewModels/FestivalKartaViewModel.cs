﻿using FestivalKarte.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FestivalKarte.ViewModels
{
    public class FestivalKartaViewModel
    {
        public Karta Karta { get; set; }
        public Festival Festival { get; set; }
        public List<Festival> Festivali { get; set; }
    }
}