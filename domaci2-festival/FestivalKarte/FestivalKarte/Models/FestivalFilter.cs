﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FestivalKarte.Models
{
    public class FestivalFilter
    {
        public string Name { get; set; }
        public string Place { get; set; }
        public DateTime? Date { get; set; }
        public decimal? Rate { get; set; }
    }
}