﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FestivalKarte.Models
{
    public class TipKarte
    {
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        [Column("Tip Karte")]
        public string Name { get; set; }

        public List<Karta> Karte { get; set; }
    }
}