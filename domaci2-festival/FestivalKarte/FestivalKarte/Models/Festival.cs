﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FestivalKarte.Models
{
    public class Festival
    {
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        [Column("Festival")]
        public string Name { get; set; }

        [Required]
        [StringLength(20)]
        public string Place { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        [Range(1,10)]
        public decimal Rate { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int MaxVisitors { get; set; }

        public List<Karta> Karte { get; set; }
    }
}