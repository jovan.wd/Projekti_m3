﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace FestivalKarte.Models
{
    public class FestivaliContext : DbContext
    {
        public DbSet<Festival> Festivali { get; set; }
        public DbSet<Karta> Karte { get; set; }
        public DbSet<TipKarte> TipoviKarata { get; set; }

        public FestivaliContext() : base("name=FestivaliContext")
        {
        }
    }
}