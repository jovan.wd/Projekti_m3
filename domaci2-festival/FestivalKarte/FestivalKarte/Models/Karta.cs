﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FestivalKarte.Models
{
    public class Karta
    {
        public int Id { get; set; }

        [Required]
        [Range(100,1000)]
        public decimal Price { get; set; }

        public DateTime PurchaseDate { get; set; }

        [Required]
        [StringLength(50)]
        public string FullName { get; set; }

        public bool Taken { get; set; }


        public int FestivalId { get; set; }

        [ForeignKey("FestivalId")]
        [Column("Festival")]
        public Festival Festival { get; set; }

        public int TipKarteId { get; set; }

        [ForeignKey("TipKarteId")]
        [Column("TipKarte")]
        public TipKarte TipKarte { get; set; }
    }
}