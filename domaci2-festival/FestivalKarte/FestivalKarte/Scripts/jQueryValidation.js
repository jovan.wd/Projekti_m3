﻿$('form').submit(function (e) {
    
    var price = $('#Karta_Price').val();
    var date = $('#Karta_PurchaseDate').val();
    var fullName = $('#Karta_FullName').val();

    $(".error").remove();

    if (price.length < 1) {
        $('#Karta_Price').after('<span class="error">This field is required</span>');
        e.preventDefault();
    }
    if (date.length < 1) {
        $('#Karta_PurchaseDate').after('<span class="error">This field is required</span>');
        e.preventDefault();
    }
    if (fullName.length < 1) {
        $('#Karta_FullName').after('<span class="error">This field is required</span>');
        e.preventDefault();
    } else {
        //var regEx = /^[A-Z0-9][A-Z0-9._%+-]{0,63}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/;
        //var validName = regEx.test(fullName);
        //if (!validName) {
        //    $('#Karta_FullName').after('<span class="error">Enter a valid Full Name</span>');
        //    e.preventDefault();
        //}
    }
    
});