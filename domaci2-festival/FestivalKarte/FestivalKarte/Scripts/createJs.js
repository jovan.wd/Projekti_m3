﻿function expandFilter() {

    var isHidden = $("#filterForm").hasClass("hidden");
    $("#filterForm").removeData();

    if (isHidden) {
        $("#filterForm").removeClass("hidden");
        $("#filterForm").removeData();
    } else {
        $("#filterForm").addClass("hidden");
    }
}

$("#filter").click(expandFilter);