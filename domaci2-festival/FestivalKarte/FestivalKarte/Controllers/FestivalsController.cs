﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FestivalKarte.Models;
using FestivalKarte.ViewModels;
using Microsoft.Ajax.Utilities;
using PagedList;

namespace FestivalKarte.Controllers
{
    public class FestivalsController : Controller
    {
        private FestivaliContext db = new FestivaliContext();

        public enum SortTypes
        {
            Name,
            NameDescending,
            MaxVisitors,
            Rate,
            RateDescending,
            Date
        };

        public static Dictionary<string, SortTypes> SortTypeDict = new Dictionary<string, SortTypes>
        {
            {"Name" , SortTypes.Name },
            {"Name Descending" , SortTypes.Name },
            {"Maximum Visitors" , SortTypes.MaxVisitors },
            {"Rate" , SortTypes.Rate },
            {"Rate Descending" , SortTypes.Rate },
            {"Date" , SortTypes.Date }
        };
        
        private readonly int FestivalsPerPage = 2;

        // GET: Festivals
        public ActionResult Index(int page = 1, string sortType = "Name")
        {
            SortTypes sortBy = SortTypeDict[sortType];

            IQueryable<Festival> festivali = db.Festivali;

            switch (sortBy)
            {
                case SortTypes.Name:
                    festivali = festivali.OrderBy(x => x.Name);
                    break;
                case SortTypes.NameDescending:
                    festivali = festivali.OrderByDescending(x => x.Name);
                    break;
                case SortTypes.MaxVisitors:
                    festivali = festivali.OrderBy(x => x.MaxVisitors);
                    break;
                case SortTypes.Rate:
                    festivali = festivali.OrderBy(x => x.Rate);
                    break;
                case SortTypes.RateDescending:
                    festivali = festivali.OrderByDescending(x => x.Rate);
                    break;
                case SortTypes.Date:
                    festivali = festivali.OrderBy(x => x.Date);
                    break;
            }

            ViewBag.sortTypes = new SelectList(SortTypeDict, "Key", "Key", sortType);
            ViewBag.CurrentSortType = sortType;
            
            return View(festivali.ToPagedList(page, FestivalsPerPage));
        }

        // GET: Festivals/Details/5
        public ActionResult Details(int? id)
        {
            var karte = db.Karte.Where(k=>k.FestivalId==id).Include( m=>m.TipKarte);
            FestivalKartaViewModel vm = new FestivalKartaViewModel();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Festival festival = db.Festivali.Find(id);
            if (festival == null)
            {
                return HttpNotFound();
            }
            vm.Festivali = new List<Festival>() { festival };
            vm.Festival = festival;
            vm.Festival.Karte = karte.ToList();
            ViewBag.FestivalId = new SelectList(vm.Festivali, "Id", "Name", id);
            ViewBag.TipKarteId = new SelectList(db.TipoviKarata, "Id", "Name");
            return View(vm);
        }
        

        [HttpPost]
        public ActionResult Details(FestivalKartaViewModel vm)
        {
            var karte = db.Karte.Where(k => k.FestivalId == vm.Karta.FestivalId).Include(m => m.TipKarte);
            vm.Festival = db.Festivali.Find(vm.Karta.FestivalId);
            Karta karta = vm.Karta;
            if(karta.FestivalId==vm.Festival.Id)
            
            if (ModelState.IsValid)
            {
                db.Karte.Add(karta);
                db.SaveChanges();
                
                vm.Festival.Karte=karte.ToList();
                vm.Karta = null;
                ViewBag.FestivalId = new SelectList(db.Festivali, "Id", "Name");
                ViewBag.TipKarteId = new SelectList(db.TipoviKarata, "Id", "Name");
                return View(vm);
            }
            return View("Nemoguce je kupiti kartu za drugi festival");
        }

        // GET: Festivals/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Festivals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Place,Date,Rate,MaxVisitors")] Festival festival)
        {
            if (ModelState.IsValid)
            {
                db.Festivali.Add(festival);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(festival);
        }

        // GET: Festivals/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Festival festival = db.Festivali.Find(id);
            if (festival == null)
            {
                return HttpNotFound();
            }
            return View(festival);
        }

        // POST: Festivals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Place,Date,Rate,MaxVisitors")] Festival festival)
        {
            if (ModelState.IsValid)
            {
                db.Entry(festival).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(festival);
        }
        
        public ActionResult Take(int id)
        {
            int festId=0;
            Karta karta = db.Karte.Find(id);

            var festivali = db.Festivali.ToList();
            foreach (var a in festivali)
            {
                if (karta.FestivalId.Equals(a.Id))
                {
                    festId += a.Id;
                }
            }

            if (karta.Taken == false)
            {
                karta.Taken = true;
                db.Entry(karta).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                RedirectToAction("Details", new { id = festId });
            }
            
            return RedirectToAction("Details", new { id=festId});
        }

        // GET: Festivals/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Festival festival = db.Festivali.Find(id);
            if (festival == null)
            {
                return HttpNotFound();
            }
            return View(festival);
        }

        // POST: Festivals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Festival festival = db.Festivali.Find(id);
            db.Festivali.Remove(festival);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Filter(FestivalFilter filter)
        {
            var festivali = db.Festivali.AsQueryable();

            if (!filter.Name.IsNullOrWhiteSpace())
            {
                festivali = festivali.Where(p => p.Name.Contains(filter.Name));
            }

            if (filter.Place != null)
            {
                festivali = festivali.Where(p => p.Place.Contains(filter.Place));
            }

            if (filter.Rate != null)
            {
                festivali = festivali.Where(p => p.Rate >= filter.Rate);
            }

            if (filter.Date != null)
            {
                festivali = festivali.Where(p => p.Date == filter.Date);
            }
            
            return View(festivali.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
