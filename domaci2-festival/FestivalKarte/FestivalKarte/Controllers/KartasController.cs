﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FestivalKarte.Models;
using FestivalKarte.ViewModels;

namespace FestivalKarte.Controllers
{
    public class KartasController : Controller
    {
        private FestivaliContext db = new FestivaliContext();

        // GET: Kartas
        public ActionResult Index()
        {
            var karte = db.Karte.Include(k => k.Festival).Include(k => k.TipKarte);
            return View(karte.ToList());
        }

        // GET: Kartas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Karta karta = db.Karte.Find(id);
            if (karta == null)
            {
                return HttpNotFound();
            }
            return View(karta);
        }

        // GET: Kartas/Create
        public ActionResult Create()
        {
            ViewBag.FestivalId = new SelectList(db.Festivali, "Id", "Name");
            ViewBag.TipKarteId = new SelectList(db.TipoviKarata, "Id", "Name");
            return View();
        }

        // POST: Kartas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Price,PurchaseDate,FullName,Taken,FestivalId,TipKarteId")] FestivalKartaViewModel vm)
        {
            Karta karta = vm.Karta;
            if (ModelState.IsValid)
            {
                db.Karte.Add(karta);
                vm.Festival.Karte.Add(karta);
                db.SaveChanges();
                return RedirectToAction("Details", "Festivals", new { id= karta.FestivalId});
            }
           
            return RedirectToAction("Index", "Festivals");
        }

        // GET: Kartas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Karta karta = db.Karte.Find(id);
            if (karta == null)
            {
                return HttpNotFound();
            }
            ViewBag.FestivalId = new SelectList(db.Festivali, "Id", "Name", karta.FestivalId);
            ViewBag.TipKarteId = new SelectList(db.TipoviKarata, "Id", "Name", karta.TipKarteId);
            return View(karta);
        }

        // POST: Kartas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Price,PurchaseDate,FullName,Taken,FestivalId,TipKarteId")] Karta karta)
        {
            int festId = 0;
            var festivali = db.Festivali.ToList();
            foreach (var a in festivali)
            {
                if (karta.FestivalId.Equals(a.Id))
                {
                    festId += a.Id;
                }
            }

            if (ModelState.IsValid)
            {
                db.Entry(karta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details", "Festivals", new { id = festId });
            }
            ViewBag.FestivalId = new SelectList(db.Festivali, "Id", "Name", karta.FestivalId);
            ViewBag.TipKarteId = new SelectList(db.TipoviKarata, "Id", "Name", karta.TipKarteId);
            return View(karta);
        }

        // GET: Kartas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Karta karta = db.Karte.Find(id);
            if (karta == null)
            {
                return HttpNotFound();
            }
            return View(karta);
        }

        // POST: Kartas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Karta karta = db.Karte.Find(id);
            db.Karte.Remove(karta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
