﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FestivalKarte.Models;
using Microsoft.Ajax.Utilities;

namespace FestivalKarte.Controllers
{
    public class TipKartesController : Controller
    {
        private FestivaliContext db = new FestivaliContext();

        public enum SortTypes
        {
            Name,
            NameDescending,
            
        };

        public static Dictionary<string, SortTypes> SortTypeDict = new Dictionary<string, SortTypes>
        {
            {"Name" , SortTypes.Name },
            {"Name Descending" , SortTypes.NameDescending }
        };

        [HttpPost]
        public ActionResult Filter(FestivalFilter filter)
        {
            var tipoviKarata = db.TipoviKarata.AsQueryable();

            if (!filter.Name.IsNullOrWhiteSpace())
            {
                tipoviKarata = tipoviKarata.Where(p => p.Name.Contains(filter.Name));
            }
       
            return View(tipoviKarata.ToList());
        }

        // GET: TipKartes
        public ActionResult Index(string sortType ="Name")
        {
            SortTypes sortBy = SortTypeDict[sortType];

            IQueryable<TipKarte> tipoviKarata = db.TipoviKarata;

            switch (sortBy)
            {
                case SortTypes.Name:
                    tipoviKarata = tipoviKarata.OrderBy(x => x.Name);
                    break;
                case SortTypes.NameDescending:
                    tipoviKarata = tipoviKarata.OrderByDescending(x => x.Name);
                    break;
            }

            ViewBag.sortTypes = new SelectList(SortTypeDict, "Key", "Key", sortType);
            ViewBag.CurrentSortType = sortType;

            return View(tipoviKarata.ToList());
        }

        // GET: TipKartes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipKarte tipKarte = db.TipoviKarata.Find(id);
            if (tipKarte == null)
            {
                return HttpNotFound();
            }
            return View(tipKarte);
        }

        // GET: TipKartes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipKartes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name")] TipKarte tipKarte)
        {
            if (ModelState.IsValid)
            {
                db.TipoviKarata.Add(tipKarte);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipKarte);
        }

        // GET: TipKartes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipKarte tipKarte = db.TipoviKarata.Find(id);
            if (tipKarte == null)
            {
                return HttpNotFound();
            }
            return View(tipKarte);
        }

        // POST: TipKartes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] TipKarte tipKarte)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipKarte).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipKarte);
        }

        // GET: TipKartes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipKarte tipKarte = db.TipoviKarata.Find(id);
            if (tipKarte == null)
            {
                return HttpNotFound();
            }
            return View(tipKarte);
        }

        // POST: TipKartes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipKarte tipKarte = db.TipoviKarata.Find(id);
            db.TipoviKarata.Remove(tipKarte);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
