namespace FestivalKarte.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Dodatavalidacijamodela : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Festivals", name: "Name", newName: "Festival");
            RenameColumn(table: "dbo.TipKartes", name: "Name", newName: "Tip Karte");
            AlterColumn("dbo.Festivals", "Festival", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Festivals", "Place", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Kartas", "FullName", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.TipKartes", "Tip Karte", c => c.String(nullable: false, maxLength: 20));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TipKartes", "Tip Karte", c => c.String());
            AlterColumn("dbo.Kartas", "FullName", c => c.String());
            AlterColumn("dbo.Festivals", "Place", c => c.String());
            AlterColumn("dbo.Festivals", "Festival", c => c.String());
            RenameColumn(table: "dbo.TipKartes", name: "Tip Karte", newName: "Name");
            RenameColumn(table: "dbo.Festivals", name: "Festival", newName: "Name");
        }
    }
}
