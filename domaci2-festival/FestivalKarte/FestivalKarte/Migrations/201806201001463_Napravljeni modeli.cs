namespace FestivalKarte.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Napravljenimodeli : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Festivals",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Place = c.String(),
                        Date = c.DateTime(nullable: false),
                        Rate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MaxVisitors = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Kartas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PurchaseDate = c.DateTime(nullable: false),
                        FullName = c.String(),
                        Taken = c.Boolean(nullable: false),
                        FestivalId = c.Int(nullable: false),
                        TipKarteId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Festivals", t => t.FestivalId, cascadeDelete: true)
                .ForeignKey("dbo.TipKartes", t => t.TipKarteId, cascadeDelete: true)
                .Index(t => t.FestivalId)
                .Index(t => t.TipKarteId);
            
            CreateTable(
                "dbo.TipKartes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Kartas", "TipKarteId", "dbo.TipKartes");
            DropForeignKey("dbo.Kartas", "FestivalId", "dbo.Festivals");
            DropIndex("dbo.Kartas", new[] { "TipKarteId" });
            DropIndex("dbo.Kartas", new[] { "FestivalId" });
            DropTable("dbo.TipKartes");
            DropTable("dbo.Kartas");
            DropTable("dbo.Festivals");
        }
    }
}
