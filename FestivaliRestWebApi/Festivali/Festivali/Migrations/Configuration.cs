namespace Festivali.Migrations
{
    using Festivali.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Festivali.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Festivali.Models.ApplicationDbContext context)
        {
            context.Mesta.AddOrUpdate(x => x.Id,
                new Mesto() { Id = 1, Naziv = "Novi Sad", PostanskiBroj = 21000 },
                new Mesto() { Id = 2, Naziv = "Budva", PostanskiBroj = 36500 },
                new Mesto() { Id = 3, Naziv = "Budapest", PostanskiBroj = 65987 }
                );

            context.Festivali.AddOrUpdate(x => x.Id,
                new Festival() { Id = 1, Naziv = "Exit", CenaKarte = 5500.50m, GodinaPrvogOdrzavanja = 1999, MestoId = 1},
                new Festival() { Id = 2, Naziv = "Sea Dance", CenaKarte = 2999.99m, GodinaPrvogOdrzavanja = 2002, MestoId = 2 },
                new Festival() { Id = 3, Naziv = "Sziget", CenaKarte = 1500.25m, GodinaPrvogOdrzavanja = 1989, MestoId = 3 }
                );
        }
    }
}
