﻿using Festivali.Interface;
using Festivali.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace Festivali.Repository
{
    public class MestoRepository : IDisposable , IMestoRepository
    {
        ApplicationDbContext db = new ApplicationDbContext();

        public IEnumerable<Mesto> GetAll()
        {
            return db.Mesta;
        }

        public Mesto GetById(int id)
        {
            return db.Mesta.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Mesto> GetByPostCode(int kod)
        {
            return db.Mesta.Where(x => x.PostanskiBroj <= kod).OrderBy(x => x.PostanskiBroj);
        }

        public void Add(Mesto mesto)
        {
            db.Mesta.Add(mesto);
            db.SaveChanges();
        }

        public void Update(Mesto mesto)
        {
            db.Entry(mesto).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public void Delete(Mesto mesto)
        {
            db.Mesta.Remove(mesto);
            db.SaveChanges();
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}