﻿using Festivali.Interface;
using Festivali.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace Festivali.Repository
{
    public class FestivalRepository : IDisposable, IFestivalRepository
    {
        ApplicationDbContext db = new ApplicationDbContext();

        public IEnumerable<Festival> GetAll()
        {
            return db.Festivali.Include(m => m.Mesto).OrderByDescending( m => m.CenaKarte);
        }

        public Festival GetById(int id)
        {
            return db.Festivali.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Festival> PostBetween(int start, int kraj)
        {
            return db.Festivali.Include( m => m.Mesto).Where(x => x.GodinaPrvogOdrzavanja <= kraj).Where(x => x.GodinaPrvogOdrzavanja >= start).OrderBy(x => x.GodinaPrvogOdrzavanja);
        }

        public void Add(Festival festival)
        {
            db.Festivali.Add(festival);
            db.SaveChanges();
        }

        public void Update(Festival festival)
        {
            db.Entry(festival).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public void Delete(Festival festival)
        {
            db.Festivali.Remove(festival);
            db.SaveChanges();
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}