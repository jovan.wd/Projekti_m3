﻿using Festivali.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Festivali.Interface
{
    public interface IMestoRepository
    {
        IEnumerable<Mesto> GetAll();
        Mesto GetById(int id);
        IEnumerable<Mesto> GetByPostCode(int kod);
        void Add(Mesto mesto);
        void Update(Mesto mesto);
        void Delete(Mesto mesto);
    }
}
