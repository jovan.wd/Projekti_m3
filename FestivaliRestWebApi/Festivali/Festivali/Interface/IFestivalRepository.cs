﻿using Festivali.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Festivali.Interface
{
    public interface IFestivalRepository
    {
        IEnumerable<Festival> GetAll();
        IEnumerable<Festival> PostBetween(int min, int max);
        Festival GetById(int id);
        void Add(Festival festival);
        void Update(Festival festival);
        void Delete(Festival festival);
    }
}
