﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Festivali.Models
{
    public class Mesto
    {
        public int Id { get; set; }

        [Required]
        public string Naziv { get; set; }

        [Range(0, 99999, ErrorMessage = "Max 5 digits")]
        public int? PostanskiBroj { get; set; }
    }
}