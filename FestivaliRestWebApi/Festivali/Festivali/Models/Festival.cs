﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Festivali.Models
{
    public class Festival
    {
        public int Id { get; set; }

        [Required]
        public string Naziv { get; set; }

        [Range(1, Int32.MaxValue)]
        public decimal CenaKarte { get; set; }

        [Range(1951,2017)]
        public int GodinaPrvogOdrzavanja { get; set; }

        // foreign key
        public int MestoId { get; set; }

        // navigation property
        public Mesto Mesto { get; set; }
    }
}