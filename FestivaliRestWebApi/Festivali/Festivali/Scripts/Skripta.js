﻿//$(window).load(function () {
//    $("#festivali").click();
//    $("#festivali").addClass("hidden");
//    $("#odjava").css("display", "none");
//    $("#registracija").css("display", "none");
//    $("#pretraga").css("display", "none");
//    $("#addNew").css("display", "none");
    
//});

$(document).ready(function () {
   
    // priprema dogadjaja
    $("body").on("click", "#btnEdit", editFestival);
    $("body").on("click", "#btnDelete", deleteFestival);

    $("#odjava").css("display", "none");
    $("#registracija").css("display", "none");
    $("#pretraga").css("display", "none");
    $("#addNew").css("display", "none");

    $("#btnReg").click(function (e) {
        $("#prijava").hide();
        $("#registracija").show();
        e.preventDefault();
    });

    // podaci od interesa
    var host = window.location.host;
    var token = null;
    var headers = {};
    var formAction = "Create";
    var editingId;

    $("#btnPretraga").click(function () {

        var start = "?start=" + $("#start").val();
        var kraj = "&kraj=" + $("#kraj").val();

        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        $.ajax({
            "type": "POST",
            "url": "http://" + host + "/api/festivali/pretraga/" + start + kraj,
            "headers": headers

        }).done(function (data, status) {
            $("#start").val('');
            $("#kraj").val('');
            console.log(data, status);
            setFestivals(data, status);

        }).fail(function (data) {
            alert(data.status + ": " + data.statusText);
        });
    });

    $("#festivali").click(function () {
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        // dobavljam sve festivale
        $.ajax({
            "type": "GET",
            "url": "http://" + host + "/api/festivali/",
            "headers": headers

        }).done(function (data, status) {
            console.log(data, status);
            setFestivals(data,status);

        }).fail(function (data) {
            alert(data.status + ": " + data.statusText);
            });


        // dobavljam gradove za popunjavanje select liste u formi za dodavanje novog festivala
        $.ajax({
            "type": "GET",
            "url": "http://" + host + "/api/mesta/",
            "headers": headers

        }).done(function (data, status) {
            console.log(data, status);
            setMesta(data, status);

        }).fail(function (data) {
            alert(data.status + ": " + data.statusText);
        });
    });

    function setMesta(data, status) {

        $('#mestoId').empty();
        if (status === "success") {
            console.log(data);
            // ispis mesta
            $('#mestoId').append("<option style='display : none'></option>");
            for (i = 0; i < data.length; i++) {
                $('#mestoId').append("<option value=" + data[i].Id + ">" + data[i].Naziv + "</option>");
                console.log(data);
            }
        }
        else {
            var h1 = $("<h1>Greška prilikom preuzimanja drzava!</h1>");
            $('#mestoId').append(h1);
        }
    }

    function setFestivals(data, status) {

        var $container = $("#data");

        $container.empty();

        if (status === "success") {
            if (token !== null) {
                // ispis naslova
                var div = $("<div></div>");
                var h1 = $("<h1>Festivali</h1><br/>");
                div.append(h1);
                // ispis tabele
                var table = $("<table border='1'  width = '500'></table>");
                var header = $("<tr bgcolor='lightgrey' style='height: 40px;'><th>Naziv</th><th>Mesto</th><th >Godina</th><th>Cena</th><th>Edit</th><th>Delete</th>");

                table.append(header);
                for (i = 0; i < data.length; i++) {
                    // prikazujemo novi red u tabeli
                    var row = "<tr style='height: 40px;'>";
                    // prikaz podataka
                    var displayData = "<td>" + data[i].Naziv + "</td><td>" + data[i].Mesto.Naziv + "</td><td >" + data[i].GodinaPrvogOdrzavanja + "</td><td>" + data[i].CenaKarte + "</td>";
                    var stringId = data[i].Id.toString();
                    var displayDelete = "<td><button style='width:100%' id=btnDelete name=" + stringId + ">Delete</button></td>";
                    var displayEdit = "<td><button style='width:100%' id=btnEdit name=" + stringId + ">Edit</button></td>";
                    row += displayData + displayDelete + displayEdit + "</tr>";
                    table.append(row);
                    newId = data[i].Id;
                }

                div.append(table);

                // ispis novog sadrzaja
                $container.append(div);
            }
            else {
                // ispis naslova
                div = $("<div></div>");
                h1 = $("<h1>Festivali</h1>");
                div.append(h1);
                // ispis tabele
                table = $("<table border='1' width = '500'></table>");
                header = $("<tr bgcolor='lightgrey' style='height: 40px;'><th>Naziv</th><th>Mesto</th><th>Godina</th><th>Cena</th><th class='hidden'>Edit</th><th class='hidden'>Delete</th>");
                table.append(header);
                for (i = 0; i < data.length; i++) {
                    // prikazujemo novi red u tabeli
                    row = "<tr style='height: 40px;'>";
                    // prikaz podataka bez dugmadi za edit i delete
                    displayData = "<td>" + data[i].Naziv + "</td><td>" + data[i].Mesto.Naziv + "</td><td>" + data[i].GodinaPrvogOdrzavanja + "</td><td>" + data[i].CenaKarte + "</td>";
                    stringId = data[i].Id.toString();
                    displayDelete = "<td class='hidden'><button class='btn btn-danger btn-sm' id=btnDelete name=" + stringId + ">Delete</button></td>";
                    displayEdit = "<td class='hidden'><button class='btn btn-warning btn-sm' id=btnEdit name=" + stringId + ">Edit</button></td>";
                    row += displayData + displayDelete + displayEdit + "</tr>";
                    table.append(row);
                    newId = data[i].Id;
                }

                div.append(table);

                // ispis novog sadrzaja
                $container.append(div);
            }

        }
        else {
            console.log(status);
            div = $("<div></div>");
            h1 = $("<h1>Greška prilikom preuzimanja festivala!</h1>");
            div.append(h1);
            $container.append(div);
        }
    }



    $("#festivali").click();

    
    // prijava korisnika
    $("#prijava").submit(function (e) {
        e.preventDefault();

        var email = $("#username").val();
        var loz = $("#password").val();

        // objekat koji se salje
        var sendData = {
            "grant_type": "password",
            "username": email,
            "password": loz
        };

        $.ajax({
            "type": "POST",
            "url": 'http://' + host + "/Token",
            "data": sendData

        }).done(function (data) {
            console.log(data);
            RefreshRegLogForm();
            $("#info").empty().append("Prijavljen korisnik: " + data.userName);
            token = data.access_token;
            $("#pretraga").css("display", "block");
            $("#prijava").hide();
            $("#registracija").hide();
            $("#odjava").css("display", "block");
            $("#addNew").show();

        }).fail(function (data) {
            alert("Pogresno korisnicko ime ili lozinka");
            RefreshRegLogForm();
            $("#username").focus();
        });
    });

    // odjava korisnika sa sistema
    $("#btnOdjava").click(function () {

        // brisemo token i headers

        token = null;
        headers = {};
        RefreshRegLogForm();
        $("#prijava").show();
        $("#registracija").hide();
        $("#pretraga").hide();
        $("#odjava").css("display", "none");
        $("#info").empty();
        $("#addNew").hide();
    });

    $("#registracija").submit(function (e) {
        e.preventDefault();

        var email = $("#username1").val();
        var loz1 = $("#password1").val();
        var loz2 = $("#password2").val();
        

        // objekat koji se salje
        var sendData = {
            "Email": email,
            "Password": loz1,
            "ConfirmPassword": loz2
        };

        $.ajax({
            type: "POST",
            url: 'http://' + host + "/api/Account/Register",
            data: sendData

        }).done(function (data) {
            RefreshRegLogForm();
            $("#prijava").show();
            $("#registracija").hide();

            }).fail(function (data) {
            alert(data);
        });
    });

    $("#addNew").submit(function (e) {
        // sprecavanje default akcije forme
        e.preventDefault();

        // dodajemo token u autorizaciju da ne bi dobili 401
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        var naziv = $("#naziv").val();
        var cenaKarte = $("#cena").val();
        var datum = $("#datum").val();
        var mestoId = $("#mestoId option:selected").val();

        var httpAction;
        var sendData;
        var url;

        // u zavisnosti od akcije pripremam objekat
        if (formAction === "Create") {
            httpAction = "POST";
            url = "http://" + host + "/api/festivali/";
            sendData = {
                "Naziv": naziv,
                "CenaKarte": cenaKarte,
                "GodinaPrvogOdrzavanja": datum,
                "MestoId": mestoId
            };
        }
        else {
            httpAction = "PUT";
            url = "http://" + host + "/api/festivali/" + editingId.toString();
            sendData = {
                "Id": editingId,
                "Naziv": naziv,
                "CenaKarte": cenaKarte,
                "GodinaPrvogOdrzavanja": datum,
                "MestoId": mestoId
            };
        }


        console.log("Objekat za slanje");
        console.log(sendData);

        $.ajax({
            url: url,
            type: httpAction,
            "headers": headers,
            data: sendData
        })
            .done(function (data, status) {
                formAction = "Create";
                refreshTable();
            })
            .fail(function (data, status) {
                alert("Desila se greska!");
            });
    });


    // brisanje festivala
    function deleteFestival() {
        // dodajemo token u autorizaciju da ne bi dobili 401
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        // izvlacimo {id}
        var deleteID = this.name;
        // saljemo zahtev 
        $.ajax({
            url: "http://" + host + "/api/festivali/" + deleteID.toString(),
            type: "DELETE",
            "headers": headers
        })
            .done(function (data, status) {
                refreshTable();
            })
            .fail(function (data, status) {
                alert("Desila se greska!");
            });

    }

    // izmena festivala
    function editFestival() {
        // dodajemo token u autorizaciju da ne bi dobili 401
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        // izvlacimo id
        var editId = this.name;
        // saljemo zahtev da dobavimo tog autora
        $.ajax({
            url: "http://" + host + "/api/festivali/" + editId.toString(),
            type: "GET",
            "headers": headers
        })
            .done(function (data, status) {
                $("#naziv").val(data.Naziv);
                $("#cena").val(data.CenaKarte);
                $("#datum").val(data.GodinaPrvogOdrzavanja);
                $("#mestoId").val(data.MestoId);
                editingId = data.Id;
                formAction = "Update";
            })
            .fail(function (data, status) {
                formAction = "Create";
                alert("Desila se greska!");
            });

    }

    function refreshTable() {
        // cistim formu
        $("#naziv").val('');
        $("#cena").val('');
        $("#datum").val('');
        $("#mestoId").val('');
        // osvezavam
        $("#festivali").trigger("click");
    }

    function RefreshRegLogForm() {
        $("#username").val('').empty();
        $("#password").val('').empty();

        $("#username1").val('');
        $("#password1").val('');
        $("#password2").val('');
        // refresujemo tabelu ako se neko u medjuvremenu prijavi

        $("#festivali").trigger("click");
    }
});