﻿using Festivali.Interface;
using Festivali.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Festivali.Controllers
{
    public class MestaController : ApiController
    {
        IMestoRepository _repository { get; set; }

        public MestaController(IMestoRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Mesto> Get()
        {
            return _repository.GetAll();
        }
        
        public IEnumerable<Mesto> GetByPostCode(int kod)
        {
            return _repository.GetByPostCode(kod);
        }

        public IHttpActionResult Get(int id)
        {
            Mesto mesto = _repository.GetById(id);

            if (mesto == null)
            {
                return NotFound();
            }

            return Ok(mesto);
        }

        public IHttpActionResult Post(Mesto mesto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _repository.Add(mesto);
            return CreatedAtRoute("DefaultApi", new { id = mesto.Id }, mesto);
        }

        public IHttpActionResult Put(int id, Mesto mesto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != mesto.Id)
            {
                return BadRequest();
            }
            try
            {
                _repository.Update(mesto);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(mesto);
        }

        public IHttpActionResult Delete(int id)
        {
            Mesto mesto = _repository.GetById(id);

            if (mesto == null)
            {
                return BadRequest();
            }

            _repository.Delete(mesto);

            return Ok();
        }
    }
}
