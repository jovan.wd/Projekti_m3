﻿using Festivali.Interface;
using Festivali.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Festivali.Controllers
{
    public class FestivaliController : ApiController
    {
        IFestivalRepository _repository { get; set; }

        public FestivaliController(IFestivalRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Festival> Get()
        {
            return _repository.GetAll();
        }

        [Authorize]
        [Route("api/festivali/pretraga/")]
        public IEnumerable<Festival> Post(int start, int kraj)
        {
            return _repository.PostBetween(start, kraj);
        }

        public IHttpActionResult Get(int id)
        {
            Festival festival = _repository.GetById(id);

            if (festival == null)
            {
                return BadRequest();
            }

            return Ok(festival);
        }

        [Authorize]
        public IHttpActionResult Post(Festival festival)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _repository.Add(festival);
            return CreatedAtRoute("DefaultApi",  new { id = festival.Id}, festival);
        }

        [Authorize]
        public IHttpActionResult Put(int id, Festival festival)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != festival.Id)
            {
                return BadRequest();
            }
            try
            {
                _repository.Update(festival);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(festival);
        }

        [Authorize]
        public IHttpActionResult Delete(int id)
        {
            Festival festival = _repository.GetById(id);

            if (festival == null)
            {
                return BadRequest();
            }

            _repository.Delete(festival);

            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}
