﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Festivali.Interface;
using Moq;
using Festivali.Models;
using Festivali.Controllers;
using System.Web.Http;
using System.Web.Http.Results;
using System.Linq;

namespace Festivali.Tests
{
    [TestClass]
    public class UnitTestMesto
    {
        [TestMethod]
        public void GetReturnsProductWithSameId()
        {
            // Arrange
            var mockRepository = new Mock<IMestoRepository>();
            mockRepository.Setup(x => x.GetById(42)).Returns(new Mesto { Id = 42 });

            var controller = new MestaController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Get(42);
            var contentResult = actionResult as OkNegotiatedContentResult<Mesto>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(42, contentResult.Content.Id);
        }

        // --------------------------------------------------------------------------------------

        [TestMethod]
        public void GetReturnsNotFound()
        {
            // Arrange
            var mockRepository = new Mock<IMestoRepository>();
            var controller = new MestaController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Get(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        //[TestMethod]
        //public void DeleteReturnsNotFound()
        //{
        //    // Arrange 
        //    var mockRepository = new Mock<IMestoRepository>();
        //    var controller = new MestaController(mockRepository.Object);

        //    // Act
        //    IHttpActionResult actionResult = controller.Delete(10);

        //    // Assert
        //    Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        //}

        // --------------------------------------------------------------------------------------

        [TestMethod]
        public void DeleteReturnsOk()
        {
            // Arrange
            var mockRepository = new Mock<IMestoRepository>();
            mockRepository.Setup(x => x.GetById(10)).Returns(new Mesto { Id = 10 });
            var controller = new MestaController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Delete(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        }

        // --------------------------------------------------------------------------------------

        [TestMethod]
        public void PutReturnsBadRequest()
        {
            // Arrange
            var mockRepository = new Mock<IMestoRepository>();
            var controller = new MestaController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Put(10, new Mesto { Id = 9, Naziv = "Mesto1" });

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        }

        // -------------------------------------------------------------------------------------

        [TestMethod]
        public void PostMethodSetsLocationHeader()
        {
            // Arrange
            var mockRepository = new Mock<IMestoRepository>();
            var controller = new MestaController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Post(new Mesto { Id = 10, Naziv = "Mesto1" });
            var createdResult = actionResult as CreatedAtRouteNegotiatedContentResult<Mesto>;

            // Assert
            Assert.IsNotNull(createdResult);
            Assert.AreEqual("DefaultApi", createdResult.RouteName);
            Assert.AreEqual(10, createdResult.RouteValues["id"]);
        }

        // ------------------------------------------------------------------------------------------

        [TestMethod]
        public void GetReturnsMultipleObjects()
        {
            // Arrange
            List<Mesto> mesta = new List<Mesto>();
            mesta.Add(new Mesto { Id = 1, Naziv = "Mesto1" });
            mesta.Add(new Mesto { Id = 2, Naziv = "Mesto2" });

            var mockRepository = new Mock<IMestoRepository>();
            mockRepository.Setup(x => x.GetAll()).Returns(mesta.AsEnumerable());
            var controller = new MestaController(mockRepository.Object);

            // Act
            IEnumerable<Mesto> result = controller.Get();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(mesta.Count, result.ToList().Count);
            Assert.AreEqual(mesta.ElementAt(0), result.ElementAt(0));
            Assert.AreEqual(mesta.ElementAt(1), result.ElementAt(1));
        }
    }
}
