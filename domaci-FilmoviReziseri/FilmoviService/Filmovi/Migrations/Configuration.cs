namespace Filmovi.Migrations
{
    using Filmovi.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Filmovi.Models.FilmoviContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Filmovi.Models.FilmoviContext context)
        {
            context.Directors.AddOrUpdate(x => x.Id,
                new Director() { Id = 1, FirstName = "Martin", LastName = "Scorsese", YearsOld = 57 },
                new Director() { Id = 2, FirstName = "Sergio", LastName = "Leon", YearsOld = 63 },
                new Director() { Id = 3, FirstName = "Steaven", LastName = "Spilberg", YearsOld = 57 }
                );

            context.Films.AddOrUpdate(x => x.Id,
                new Film()
                {
                    Id = 1,
                    Name = "Saving Private Ryan",
                    Genre = "War",
                    Year = 1998,
                    DirectorId = 3
                },
                new Film()
                {
                    Id = 2,
                    Name = "Catch Me If You Can",
                    Genre = "Triller",
                    Year = 2002,
                    DirectorId = 3
                },
                new Film()
                {
                    Id = 3,
                    Name = "Cape Fear",
                    Genre = "Mistery",
                    Year = 1991,
                    DirectorId = 1
                },
                new Film()
                {
                    Id = 4,
                    Name = "The Good, The Bad, And The Ugly",
                    Genre = "Western",
                    Year = 1966,
                    DirectorId = 3
                }
                );
        }
    }
}
