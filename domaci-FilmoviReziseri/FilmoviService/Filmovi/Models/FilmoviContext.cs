﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Filmovi.Models
{
    public class FilmoviContext : DbContext
    {
        public DbSet<Film> Films { get; set; }
        public DbSet<Director> Directors { get; set; }

        public FilmoviContext() : base("name = FilmoviContext")
        {

        }
    }
}