﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Filmovi.Models
{
    public class Film
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        public string Genre { get; set; }
        public int Year { get; set; }

        // foreign key
        public int DirectorId { get; set; }
        // navigation property
        public Director Director { get; set; }
    }
}