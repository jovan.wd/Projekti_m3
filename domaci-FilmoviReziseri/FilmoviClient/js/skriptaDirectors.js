$(document).ready(function(){

	// podaci od interesa
	var host = "http://localhost:";
	var port = "51351/";
	var directorsEndpoint = "api/directors/";
	var formAction = "Create";
	var editingId;

	// pripremanje dogadjaja za brisanje
	$("body").on("click", "#btnDelete", deleteDirector);

	// priprema dogadjaja za izmenu
	$("body").on("click", "#btnEdit", editDirector);

	// prikaz autora
	$("#btnDirectors").click(function(){
		// ucitavanje autora
		var requestUrl = host + port + directorsEndpoint;
		console.log("URL zahteva: " + requestUrl);
		$.getJSON(requestUrl, setDirectors);
	});

	// metoda za postavljanje autora u tabelu
	function setDirectors(data, status){
		console.log("Status: " + status);

		var $container = $("#dataD");
		$container.empty(); 
		
		if (status == "success")
		{
			console.log(data);
			// ispis naslova
			var div = $("<div></div>");
			var h1 = $("<h1>Režiseri</h1>");
			div.append(h1);
			// ispis tabele
			var table = $("<table border='1' class='table' style='background-color: lightgrey'></table>");
			var header = $("<tr><th align='center'>Id</th><th align='center'>FirstName</th><th align='center'>LastName</th><th align='center'>YearsOld</th><th align='center'>Delete</th><th align='center'>Edit</th></tr>");
			table.append(header);
			for (i=0; i<data.length; i++)
			{
				// prikazujemo novi red u tabeli
				var row = "<tr>";
				// prikaz podataka
				var displayData = "<td align='center'>" + data[i].Id + "</td><td align='center'>" + data[i].FirstName + "</td><td align='center'>" + data[i].LastName + "</td><td align='center'>" + data[i].YearsOld + "</td>";
				// prikaz dugmadi za izmenu i brisanje
				var stringId = data[i].Id.toString();
				var displayDelete = "<td><button class='btn btn-danger btn-sm' id=btnDelete name=" + stringId + ">Delete</button></td>";
				var displayEdit = "<td><button class='btn btn-warning btn-sm' id=btnEdit name=" + stringId + ">Edit</button></td>";
				row += displayData + displayDelete + displayEdit + "</tr>";  
				table.append(row);
				newId = data[i].Id;
			}
			
			div.append(table);

			// prikaz forme
			$("#formDivD").css("display","block");

			// ispis novog sadrzaja
			$container.append(div);
		}
		else 
		{
			var div = $("<div></div>");
			var h1 = $("<h1>Greška prilikom preuzimanja Rezisera!</h1>");
			div.append(h1);
			$container.append(div);
		}
	};

	// dodavanje novog autora
	$("#directorForm").submit(function(e){
		// sprecavanje default akcije forme
		e.preventDefault();

		var directorFirstName = $("#directorFirstName").val();
		var directorLastName = $("#directorLastName").val();
		var directorYearsOld = $("#directorYearsOld").val();
		var httpAction;
		var sendData;
		var url;

		// u zavisnosti od akcije pripremam objekat
		if (formAction === "Create") {
			httpAction = "POST";
			url = host + port + directorsEndpoint;
			sendData = {
				"FirstName": directorFirstName,
				"LastName": directorLastName,
				"YearsOld": directorYearsOld
			};
		}
		else {
			httpAction = "PUT";
			url = host + port + directorsEndpoint + editingId.toString();
			sendData = {
				"Id": editingId,
				"FirstName": directorFirstName,
				"LastName": directorLastName,
				"YearsOld": directorYearsOld
			};
		}
		

		console.log("Objekat za slanje");
		console.log(sendData);

		$.ajax({
			url: url,
			type: httpAction,
			data: sendData
		})
		.done(function(data, status){
			formAction = "Create";
			refreshTable();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		})

	});

	// brisanje autora
	function deleteDirector() {
		// izvlacimo {id}
		var deleteID = this.name;
		// saljemo zahtev 
		$.ajax({
			url: host + port + directorsEndpoint + deleteID.toString(),
			type: "DELETE",
		})
		.done(function(data, status){
			refreshTable();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		});

	};

	// izmena autora
	function editDirector(){
		// izvlacimo id
		var editId = this.name;
		// saljemo zahtev da dobavimo tog autora
		$.ajax({
			url: host + port + directorsEndpoint + editId.toString(),
			type: "GET",
		})
		.done(function(data, status){
			$("#directorFirstName").val(data.FirstName);
			$("#directorLastName").val(data.LastName);
			$("#directorYearsOld").val(data.YearsOld);
			editingId = data.Id;
			formAction = "Update";
		})
		.fail(function(data, status){
			formAction = "Create";
			alert("Desila se greska!");
		});

	};

	// osvezi prikaz tabele
	function refreshTable() {
		// cistim formu
		$("#directorFirstName").val('');
		$("#directorLastName").val('');
		$("#directorYearsOld").val('');
		// osvezavam
		$("#btnDirectors").trigger("click");
	};

});
