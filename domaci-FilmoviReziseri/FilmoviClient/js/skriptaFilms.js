$(document).ready(function(){

	// podaci od interesa
	var host = "http://localhost:";
	var port = "51351/";
	var filmsEndpoint = "api/films/";
	var formAction = "Create";
	var editingId;

	// pripremanje dogadjaja za brisanje
	$("body").on("click", "#btnDelete1", deleteFilm);

	// priprema dogadjaja za izmenu
	$("body").on("click", "#btnEdit1", editFilm);

	// prikaz autora
	$("#btnFilms").click(function(){
		// ucitavanje autora
		var requestUrl = host + port + filmsEndpoint;
		console.log("URL zahteva: " + requestUrl);
		$.getJSON(requestUrl, setFilms);
	});

	// metoda za postavljanje autora u tabelu
	function setFilms(data, status){
		console.log("Status: " + status);

		var $container = $("#dataF");
		$container.empty(); 
		
		if (status == "success")
		{
			console.log(data);
			// ispis naslova
			var div = $("<div></div>");
			var h1 = $("<h1>Filmovi</h1>");
			div.append(h1);
			// ispis tabele
			var table = $("<table border='1' class='table' style='background-color: lightgrey'></table>");
			var header = $("<tr><th align='center'>Id</th><th align='center'>Name</th><th align='center'>Genre</th><th align='center'>Year</th><th align='center'>Director</th><th align='center'>Delete</th><th align='center'>Edit</th></tr>");
			table.append(header);
			for (i=0; i<data.length; i++)
			{
				// prikazujemo novi red u tabeli
				var row = "<tr>";
				// prikaz podataka
				var displayData = "<td align='center'>" + data[i].Id + "</td><td align='center'>" + data[i].Name + "</td><td align='center'>" + data[i].Genre + "</td><td align='center'>" + data[i].Year + "</td><td align='center'>"+ data[i].DirectorId + "</td>";
				// prikaz dugmadi za izmenu i brisanje
				var stringId = data[i].Id.toString();
				var displayDelete = "<td><button class='btn btn-danger btn-sm' id=btnDelete1 name=" + stringId + ">Delete</button></td>";
				var displayEdit = "<td><button class='btn btn-warning btn-sm' id=btnEdit1 name=" + stringId + ">Edit</button></td>";
				row += displayData + displayDelete + displayEdit + "</tr>";  
				table.append(row);
				newId = data[i].Id;
			}
			
			div.append(table);

			// prikaz forme
			$("#formDivF").css("display","block");

			// ispis novog sadrzaja
			$container.append(div);
		}
		else 
		{
			var div = $("<div></div>");
			var h1 = $("<h1>Greška prilikom preuzimanja Filmova!</h1>");
			div.append(h1);
			$container.append(div);
		}
	};

	// dodavanje novog autora
	$("#filmForm").submit(function(e){
		// sprecavanje default akcije forme
		e.preventDefault();

		var filmName = $("#filmName").val();
		var filmGenre = $("#filmGenre").val();
		var filmYear = $("#filmYear").val();
		var filmDirector = $("#filmDirector").val();
		var httpAction;
		var sendData;
		var url;

		// u zavisnosti od akcije pripremam objekat
		if (formAction === "Create") {
			httpAction = "POST";
			url = host + port + filmsEndpoint;
			sendData = {
				"Name": filmName,
				"Genre": filmGenre,
				"Year": filmYear,
				"DirectorId": filmDirector
			};
		}
		else {
			httpAction = "PUT";
			url = host + port + filmsEndpoint + editingId.toString();
			sendData = {
				"Id": editingId,
				"Name": filmName,
				"Genre": filmGenre,
				"Year": filmYear,
				"DirectorId": filmDirector
			};
		}
		

		console.log("Objekat za slanje");
		console.log(sendData);

		$.ajax({
			url: url,
			type: httpAction,
			data: sendData
		})
		.done(function(data, status){
			formAction = "Create";
			refreshTable();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		})

	});

	// brisanje autora
	function deleteFilm() {
		// izvlacimo {id}
		var deleteID = this.name;
		// saljemo zahtev 
		$.ajax({
			url: host + port + filmsEndpoint + deleteID.toString(),
			type: "DELETE",
		})
		.done(function(data, status){
			refreshTable();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		});

	};

	// izmena autora
	function editFilm(){
		// izvlacimo id
		var editId = this.name;
		// saljemo zahtev da dobavimo tog autora
		$.ajax({
			url: host + port + filmsEndpoint + editId.toString(),
			type: "GET",
		})
		.done(function(data, status){
			$("#filmName").val(data.Name);
			$("#filmGenre").val(data.Genre);
			$("#filmYear").val(data.Year);
			$("#filmDirector").val(data.DirectorId);
			editingId = data.Id;
			formAction = "Update";
		})
		.fail(function(data, status){
			formAction = "Create";
			alert("Desila se greska!");
		});

	};

	// osvezi prikaz tabele
	function refreshTable() {
		// cistim formu
		$("#filmName").val('');
		$("#filmGenre").val('');
		$("#filmYear").val('');
		$("#filmDirector").val('');
		// osvezavam
		$("#btnFilms").trigger("click");
	};

});
