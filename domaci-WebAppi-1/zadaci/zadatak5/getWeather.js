
$(document).ready(function () {
    $("#btn1").click(function () {

        var city = $("#city").val();
        var zip =  $("#zip").val();
        var cityId = $("#cityId").val();
        var appid = "&APPID=3726fe9cef4ab9d506ee77d8d8f7f3d1";
        if (city != "" && zip == "" && cityId =="") {
            $.ajax({
                url: 'http://api.openweathermap.org/data/2.5/weather?q=' + city + "&units=metric" + appid,
                type: "get",
                dataType: "jsonp",
                success: function (data) {
                    var text = showResults(data);
                    $("#div1").html(text);
                    $("#city").val('');
                }
            })
        }
        else if(city == "" && zip == "" && cityId !=""){
            $.ajax({
                url: 'http://api.openweathermap.org/data/2.5/weather?id=' + cityId + "&units=metric" + appid,
                type: "get",
                dataType: "jsonp",
                success: function (data) {
                    var text = showResults(data);
                    $("#div1").html(text);
                    $("#cityId").val('');
                }
            })
        }
        else if(city == "" && zip != "" && cityId ==""){
            $.ajax({
                url: 'http://api.openweathermap.org/data/2.5/weather?zip=' + zip + "&units=metric" + appid,
                type: "get",
                dataType: "jsonp",
                success: function (data) {
                    var text = showResults(data);
                    $("#div1").html(text);
                    $("#zip").val('');
                }
            })
        }
    })

    function showResults(data) {
        return '<h2>Temperature for ' + data.name + ', ' + data.sys.country + '</h2>' +
            '<h3>City Id: ' + data.id + '</h3>' +
            '<h3>Weather: ' + data.weather[0].main + '</h3>' +
            '<h3>Current temp : ' + data.main.temp + '&#8451;</h3>' +
            '<h3>MinTemp : ' + data.main.temp_min + '&#8451;</h3>' +
            '<h3>MaxTemp : ' + data.main.temp_max + '&#8451;</h3>' +
            '<h3>Pressure : ' + data.main.pressure + 'hpa</h3>' +
            '<h3>Humidity : ' + data.main.humidity + '%</h3>'
    }

});
