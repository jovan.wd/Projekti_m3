﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AutoSaloni.Models
{
    public class Auto
    {
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        public string Model { get; set; }

        [Required]
        [Range(1970, 2018)]
        public int Year { get; set; }

        [Required]
        [Range(750, 4000)]
        public int Volume { get; set; }

        [Required]
        [StringLength(20)]
        public string Color { get; set; }

        [Column("Proizvodjac")]
        public int ProizvodjacId { get; set; }

        [ForeignKey("ProizvodjacId")]
        public Proizvodjac Proizvodjac { get; set; }

        [Column("Salon")]
        public int SalonId { get; set; }

        [ForeignKey("SalonId")]
        public Salon Salon { get; set; }

    }
}