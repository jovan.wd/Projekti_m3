﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AutoSaloni.Models
{
    public class Salon
    {
        public int Id { get; set; }

        [Required]
        [Range(100000, 999999)]
        public int PIB { get; set; }

        [Required]
        [StringLength(20)]
        [Column("Salon")]
        public string Name { get; set; }

        [Required]
        [StringLength(20)]
        public string Country { get; set; }

        [Required]
        [StringLength(20)]
        public string City { get; set; }

        [Required]
        [StringLength(50)]
        public string Adress { get; set; }

        public List<Auto> Automobili { get; set; }
        public List<Ugovor> Ugovori { get; set; }

    }
}