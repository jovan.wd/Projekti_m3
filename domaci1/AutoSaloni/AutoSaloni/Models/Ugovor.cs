﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AutoSaloni.Models
{
    public class Ugovor
    {
        public int Id { get; set; }

        [Column("Proizvodjac")]
        public int ProizvodjacId { get; set; }

        [ForeignKey("ProizvodjacId")]
        public Proizvodjac Proizvodjac { get; set; }

        [Column("Salon")]
        public int SalonId { get; set; }

        [ForeignKey("SalonId")]
        public Salon Salon { get; set; }
    }
}