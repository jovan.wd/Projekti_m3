﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AutoSaloni.Models
{
    public class AutoSalonContext : DbContext
    {
        public DbSet<Salon> Saloni { get; set; }
        public DbSet<Auto> Auti { get; set; }
        public DbSet<Proizvodjac> Proizvodjaci { get; set; }
        public DbSet<Ugovor> Ugovori { get; set; }

        public AutoSalonContext() : base("name = AutoSaloniContext")
        {
        }
    }
}