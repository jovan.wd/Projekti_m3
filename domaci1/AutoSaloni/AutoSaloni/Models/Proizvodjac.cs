﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AutoSaloni.Models
{
    public class Proizvodjac
    {
        public int Id { get; set; }
        
        [Required]
        [StringLength(20)]
        [Column("Proizvodjac")]
        public string Name { get; set; }

        [Required]
        [StringLength(20)]
        public string Country { get; set; }

        [Required]
        [StringLength(20)]
        public string City { get; set; }

        public List<Auto> Automobili { get; set; }
        public List<Ugovor> Ugovori { get; set; }
    }
}