namespace AutoSaloni.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Dodatinazivikolona : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Autoes", name: "ProizvodjacId", newName: "Proizvodjac");
            RenameColumn(table: "dbo.Autoes", name: "SalonId", newName: "Salon");
            RenameIndex(table: "dbo.Autoes", name: "IX_ProizvodjacId", newName: "IX_Proizvodjac");
            RenameIndex(table: "dbo.Autoes", name: "IX_SalonId", newName: "IX_Salon");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Autoes", name: "IX_Salon", newName: "IX_SalonId");
            RenameIndex(table: "dbo.Autoes", name: "IX_Proizvodjac", newName: "IX_ProizvodjacId");
            RenameColumn(table: "dbo.Autoes", name: "Salon", newName: "SalonId");
            RenameColumn(table: "dbo.Autoes", name: "Proizvodjac", newName: "ProizvodjacId");
        }
    }
}
