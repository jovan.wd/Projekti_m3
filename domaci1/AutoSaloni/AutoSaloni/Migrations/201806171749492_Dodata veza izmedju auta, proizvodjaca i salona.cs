namespace AutoSaloni.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Dodatavezaizmedjuautaproizvodjacaisalona : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Autoes", "Proizvodjac_Id", "dbo.Proizvodjacs");
            DropForeignKey("dbo.Autoes", "Salon_Id", "dbo.Salons");
            DropIndex("dbo.Autoes", new[] { "Proizvodjac_Id" });
            DropIndex("dbo.Autoes", new[] { "Salon_Id" });
            RenameColumn(table: "dbo.Autoes", name: "Proizvodjac_Id", newName: "ProizvodjacId");
            RenameColumn(table: "dbo.Autoes", name: "Salon_Id", newName: "SalonId");
            AlterColumn("dbo.Autoes", "ProizvodjacId", c => c.Int(nullable: false));
            AlterColumn("dbo.Autoes", "SalonId", c => c.Int(nullable: false));
            CreateIndex("dbo.Autoes", "ProizvodjacId");
            CreateIndex("dbo.Autoes", "SalonId");
            AddForeignKey("dbo.Autoes", "ProizvodjacId", "dbo.Proizvodjacs", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Autoes", "SalonId", "dbo.Salons", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Autoes", "SalonId", "dbo.Salons");
            DropForeignKey("dbo.Autoes", "ProizvodjacId", "dbo.Proizvodjacs");
            DropIndex("dbo.Autoes", new[] { "SalonId" });
            DropIndex("dbo.Autoes", new[] { "ProizvodjacId" });
            AlterColumn("dbo.Autoes", "SalonId", c => c.Int());
            AlterColumn("dbo.Autoes", "ProizvodjacId", c => c.Int());
            RenameColumn(table: "dbo.Autoes", name: "SalonId", newName: "Salon_Id");
            RenameColumn(table: "dbo.Autoes", name: "ProizvodjacId", newName: "Proizvodjac_Id");
            CreateIndex("dbo.Autoes", "Salon_Id");
            CreateIndex("dbo.Autoes", "Proizvodjac_Id");
            AddForeignKey("dbo.Autoes", "Salon_Id", "dbo.Salons", "Id");
            AddForeignKey("dbo.Autoes", "Proizvodjac_Id", "dbo.Proizvodjacs", "Id");
        }
    }
}
