namespace AutoSaloni.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Izmenjeninazivipoljautablamaproizvodjacisalon : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Proizvodjacs", name: "Name", newName: "Proizvodjac");
            RenameColumn(table: "dbo.Salons", name: "Name", newName: "Salon");
        }
        
        public override void Down()
        {
            RenameColumn(table: "dbo.Salons", name: "Salon", newName: "Name");
            RenameColumn(table: "dbo.Proizvodjacs", name: "Proizvodjac", newName: "Name");
        }
    }
}
