namespace AutoSaloni.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Izmenjeninazivpoljautabeliugovor : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Ugovors", name: "ProizvodjacId", newName: "Proizvodjac");
            RenameColumn(table: "dbo.Ugovors", name: "SalonId", newName: "Salon");
            RenameIndex(table: "dbo.Ugovors", name: "IX_ProizvodjacId", newName: "IX_Proizvodjac");
            RenameIndex(table: "dbo.Ugovors", name: "IX_SalonId", newName: "IX_Salon");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Ugovors", name: "IX_Salon", newName: "IX_SalonId");
            RenameIndex(table: "dbo.Ugovors", name: "IX_Proizvodjac", newName: "IX_ProizvodjacId");
            RenameColumn(table: "dbo.Ugovors", name: "Salon", newName: "SalonId");
            RenameColumn(table: "dbo.Ugovors", name: "Proizvodjac", newName: "ProizvodjacId");
        }
    }
}
