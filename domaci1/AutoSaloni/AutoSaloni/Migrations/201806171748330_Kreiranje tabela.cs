namespace AutoSaloni.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Kreiranjetabela : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Autoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Model = c.String(nullable: false, maxLength: 20),
                        Year = c.Int(nullable: false),
                        Volume = c.Int(nullable: false),
                        Color = c.String(nullable: false, maxLength: 20),
                        Proizvodjac_Id = c.Int(),
                        Salon_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Proizvodjacs", t => t.Proizvodjac_Id)
                .ForeignKey("dbo.Salons", t => t.Salon_Id)
                .Index(t => t.Proizvodjac_Id)
                .Index(t => t.Salon_Id);
            
            CreateTable(
                "dbo.Proizvodjacs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 20),
                        Country = c.String(nullable: false, maxLength: 20),
                        City = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Salons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PIB = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 20),
                        Country = c.String(nullable: false, maxLength: 20),
                        City = c.String(nullable: false, maxLength: 20),
                        Adress = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Ugovors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProizvodjacId = c.Int(nullable: false),
                        SalonId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Proizvodjacs", t => t.ProizvodjacId, cascadeDelete: true)
                .ForeignKey("dbo.Salons", t => t.SalonId, cascadeDelete: true)
                .Index(t => t.ProizvodjacId)
                .Index(t => t.SalonId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Ugovors", "SalonId", "dbo.Salons");
            DropForeignKey("dbo.Ugovors", "ProizvodjacId", "dbo.Proizvodjacs");
            DropForeignKey("dbo.Autoes", "Salon_Id", "dbo.Salons");
            DropForeignKey("dbo.Autoes", "Proizvodjac_Id", "dbo.Proizvodjacs");
            DropIndex("dbo.Ugovors", new[] { "SalonId" });
            DropIndex("dbo.Ugovors", new[] { "ProizvodjacId" });
            DropIndex("dbo.Autoes", new[] { "Salon_Id" });
            DropIndex("dbo.Autoes", new[] { "Proizvodjac_Id" });
            DropTable("dbo.Ugovors");
            DropTable("dbo.Salons");
            DropTable("dbo.Proizvodjacs");
            DropTable("dbo.Autoes");
        }
    }
}
