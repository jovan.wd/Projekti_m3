﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoSaloni.Models;

namespace AutoSaloni.Controllers
{
    public class SalonsController : Controller
    {
        private AutoSalonContext db = new AutoSalonContext();

        // GET: Salons
        public ActionResult Index()
        {
            return View(db.Saloni.ToList());
        }

        // GET: Salons/Details/5
        public ActionResult Details(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                Salon salon = db.Saloni.Find(id);

                var ugovor = db.Ugovori.ToList().Where(c => c.SalonId.Equals(id));

                List<int> proId= new List<int>();
                foreach (var u in ugovor)
                {
                    proId.Add(u.ProizvodjacId);
                }
                
                if (ugovor.Count() != 0)
                {
                    var automobili = db.Auti.Where(a => a.SalonId == id && proId.Contains(a.ProizvodjacId)).Include(a => a.Proizvodjac);
                    salon.Automobili = automobili.ToList();
                    return View(salon);
                }

                return View(salon);
            }
            catch
            {
                return View();
            }

        }

        // GET: Salons/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Salons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PIB,Name,Country,City,Adress")] Salon salon)
        {
            if (ModelState.IsValid)
            {
                db.Saloni.Add(salon);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(salon);
        }

        // GET: Salons/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Salon salon = db.Saloni.Find(id);
            if (salon == null)
            {
                return HttpNotFound();
            }
            return View(salon);
        }

        // POST: Salons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PIB,Name,Country,City,Adress")] Salon salon)
        {
            if (ModelState.IsValid)
            {
                db.Entry(salon).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(salon);
        }

        // GET: Salons/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Salon salon = db.Saloni.Find(id);
            if (salon == null)
            {
                return HttpNotFound();
            }
            return View(salon);
        }

        // POST: Salons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Salon salon = db.Saloni.Find(id);
            db.Saloni.Remove(salon);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
