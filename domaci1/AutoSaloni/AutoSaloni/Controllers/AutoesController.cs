﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoSaloni.Models;

namespace AutoSaloni.Controllers
{
    public class AutoesController : Controller
    {
        private AutoSalonContext db = new AutoSalonContext();

        // GET: Autoes
        public ActionResult Index()
        {
            var auti = db.Auti.Include(a => a.Proizvodjac).Include(a => a.Salon);
            return View(auti.ToList());
        }

        // GET: Autoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Auto auto = db.Auti.Find(id);
            if (auto == null)
            {
                return HttpNotFound();
            }
            return View(auto);
        }

        // GET: Autoes/Create
        public ActionResult Create(int id)
        {
            ViewBag.ProizvodjacId = new SelectList(db.Proizvodjaci, "Id", "Name");
            ViewBag.SalonId = new SelectList(db.Saloni, "Id", "Name");
            return View();
        }

        // POST: Autoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Model,Year,Volume,Color,ProizvodjacId,SalonId")] Auto auto)
        {
            if (ModelState.IsValid)
            {
                db.Auti.Add(auto);
                var ugovori = db.Ugovori.ToList().Where(u=>u.ProizvodjacId==auto.ProizvodjacId && u.SalonId==auto.SalonId);

                if (ugovori != null)
                {
                    db.SaveChanges();
                    return RedirectToAction("Index", "Salons");
                }
                
            }

            ViewBag.ProizvodjacId = new SelectList(db.Proizvodjaci, "Id", "Name", auto.ProizvodjacId);
            ViewBag.SalonId = new SelectList(db.Saloni, "Id", "Name", auto.SalonId);
            
            return View(auto);
        }

        // GET: Autoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Auto auto = db.Auti.Find(id);
            if (auto == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProizvodjacId = new SelectList(db.Proizvodjaci, "Id", "Name", auto.ProizvodjacId);
            ViewBag.SalonId = new SelectList(db.Saloni, "Id", "Name", auto.SalonId);
            return View(auto);
        }

        // POST: Autoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Model,Year,Volume,Color,ProizvodjacId,SalonId")] Auto auto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(auto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProizvodjacId = new SelectList(db.Proizvodjaci, "Id", "Name", auto.ProizvodjacId);
            ViewBag.SalonId = new SelectList(db.Saloni, "Id", "Name", auto.SalonId);
            return View(auto);
        }

        // GET: Autoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Auto auto = db.Auti.Find(id);
            if (auto == null)
            {
                return HttpNotFound();
            }
            return View(auto);
        }

        // POST: Autoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Auto auto = db.Auti.Find(id);
            db.Auti.Remove(auto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
