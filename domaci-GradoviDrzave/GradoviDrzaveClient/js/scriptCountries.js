$(document).ready(function(){

	// podaci od interesa
	var host = "http://localhost:";
	var port = "61460/";
	var countryEndpoint = "api/countries/";
	var statisticsEndpoint = "api/statistics";
	var formAction = "Create";
	var editingId;

	// pripremanje dogadjaja za brisanje
	$("body").on("click", "#btnDelete", deleteCountry);

	// priprema dogadjaja za izmenu
	$("body").on("click", "#btnEdit", editCountry);

	// prikaz drzava
	$("#btnCountries").click(function(){
		// ucitavanje drzava
		var requestUrl = host + port + countryEndpoint;
		console.log("URL zahteva: " + requestUrl);
		$.getJSON(requestUrl, setCountries);
	});

	$("#btnStat").click(function() {

		var requestUrl = host+port+statisticsEndpoint;
		console.log("URL zahteva: " + requestUrl);
		$.getJSON(requestUrl, setCountries)
	});
	// metoda za postavljanje drzava u tabelu
	function setCountries(data, status){
		console.log("Status: " + status);

		var $container = $("#dataCountry");
		$container.empty(); 
		
		if (status == "success")
		{
			console.log(data);
			// ispis naslova
			var div = $("<div></div>");
			var h1 = $("<h1>Countries</h1>");
			div.append(h1);
			// ispis tabele
			var table = $("<table border='1' class='table' style='background-color: #b3b3cc'></table>");
			var header = $("<tr><th align='center'>Id</th><th align='center'>Country</th><th align='center'>International Code</th><th align='center'>Delete</th><th align='center'>Edit</th></tr>");
			table.append(header);
			for (i=0; i<data.length; i++)
			{
				// prikazujemo novi red u tabeli
				var row = "<tr class='hh'>";
				// prikaz podataka
				var displayData = "<td align='center'>" + data[i].Id + "</td><td align='center'>" + data[i].Name + "</td><td align='center'>" + data[i].InternationalCode + "</td>";
				// prikaz dugmadi za izmenu i brisanje
				var stringId = data[i].Id.toString();
				var displayDelete = "<td><button class='btn btn-danger btn-sm' id=btnDelete name=" + stringId + ">Delete</button></td>";
				var displayEdit = "<td><button class='btn btn-warning btn-sm' id=btnEdit name=" + stringId + ">Edit</button></td>";
				row+=displayData+displayDelete+displayEdit+ "</tr>";
				table.append(row);
				newId = data[i].Id;
			}
			
			div.append(table);

			// prikaz forme
			$("#formDivCountry").css("display","block");

			// ispis novog sadrzaja
			$container.append(div);
		}
		else 
		{
			var div = $("<div></div>");
			var h1 = $("<h1>Greška prilikom preuzimanja drzava!</h1>");
			div.append(h1);
			$container.append(div);
		}
	};

	// dodavanje nove drzave
	$("#countryForm").submit(function(e){
		// sprecavanje default akcije forme
		e.preventDefault();

		var countryName = $("#countryName").val();
		var countryIC = $("#countryIC").val();
		var httpAction;
		var sendData;
		var url;

		// u zavisnosti od akcije pripremam objekat
		if (formAction === "Create") {
			httpAction = "POST";
			url = host + port + countryEndpoint;
			sendData = {
				"Name": countryName,
				"InternationalCode": countryIC
			};
		}
		else {
			httpAction = "PUT";
			url = host + port + countryEndpoint + editingId.toString();
			sendData = {
				"Id": editingId,
				"Name": countryName,
				"InternationalCode": countryIC
			};
		}
		

		console.log("Objekat za slanje");
		console.log(sendData);

		$.ajax({
			url: url,
			type: httpAction,
			data: sendData
		})
		.done(function(data, status){
			formAction = "Create";
			refreshTable();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		})

	});

	// brisanje drzave
	function deleteCountry() {
		// izvlacimo {id}
		var deleteID = this.name;
		// saljemo zahtev 
		$.ajax({
			url: host + port + countryEndpoint + deleteID.toString(),
			type: "Delete",
		})
		.done(function(data, status){
			refreshTable();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		});

	};

	// izmena autora
	function editCountry(){
		// izvlacimo id
		var editId = this.name;
		// saljemo zahtev da dobavimo tog autora
		$.ajax({
			url: host + port + countryEndpoint + editId.toString(),
			type: "GET",
		})
		.done(function(data, status){
			$("#countryName").val(data.Name);
			$("#countryIC").val(data.InternationalCode);
			editingId = data.Id;
			formAction = "Update";
		})
		.fail(function(data, status){
			formAction = "Create";
			alert("Desila se greska!");
		});

	};

	// osvezi prikaz tabele
	function refreshTable() {
		// cistim formu
		$("#countryName").val('');
		$("#countryIC").val('');
		// osvezavam
		$("#btnCountries").trigger("click");
		$("#btnCities").trigger("click");
	};

});
