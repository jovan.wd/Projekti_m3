


$(document).ready(function(){

	// podaci od interesa
	var host = "http://localhost:";
	var port = "61460/";
	var cityEndpoint = "api/cities/";
	var countryEndpoint = "api/countries/";
	var formAction = "Create";
	var editingId;

	// priprema dugmeta za prikaz Search forme
	$("#btnSearch").click(expandForm);

	// pripremanje dogadjaja za brisanje
	$("body").on("click", "#btnDelete1", deleteCity);

	// priprema dogadjaja za izmenu
	$("body").on("click", "#btnEdit1", editCity);

	// prikaz autora
	$("#btnCities").click(function(){
		// ucitavanje gradova
		var requestUrl = host + port + cityEndpoint;
		var requestUrlCountry = host + port + countryEndpoint;
		console.log("URL zahteva: " + requestUrl);
		$.getJSON(requestUrl, setCities);
		$.getJSON(requestUrlCountry, setCountry);
	});

	// metoda za prikaz forme za pretragu gradova po broju stanovnika
	function expandForm() {

		var isHidden = $("#formSearch").hasClass("hidden");
		$("#formSearch").removeData();
	
		if (isHidden) {
			$("#formSearch").removeClass("hidden");
			$("#formSearch").removeData();
		} else {
			$("#formSearch").addClass("hidden");
		}
	}


	$("#btnSearchCities").click(function(){
		
		var minPop = "?min=" +  $("#minPop").val(); 
		var maxPop = "&max=" +  $("#maxPop").val();
	
		// ucitavanje gradova
		var requestUrlPop = host + port + cityEndpoint + minPop + maxPop;
		console.log("URL zahteva: " + requestUrlPop);
		$.getJSON(requestUrlPop, setCities);
		refreshSearch();
	});


	// metoda za postavljanje autora u tabelu
	function setCities(data, status){
		console.log("Status: " + status);

		var $container = $("#dataCity");
		$container.empty(); 
		
		if (status == "success")
		{
			console.log(data);
			// ispis naslova
			var div = $("<div></div>");
			var h1 = $("<h1>Cities</h1>");
			div.append(h1);
			// ispis tabele
			var table = $("<table border='1' class='table' style='background-color: b3b3cc'></table>");
			var header = $("<tr><th align='center'>Id</th><th align='center'>City</th><th align='center'>Post Code</th><th align='center'>Population</th><th align='center'>Country</th><th align='center'>Delete</th><th align='center'>Edit</th></tr>");
			table.append(header);
			for (i=0; i<data.length; i++)
			{
				// prikazujemo novi red u tabeli
				var row = "<tr class='hh'>";
				// prikaz podataka
				var displayData = "<td align='center'>" + data[i].Id + "</td><td align='center'>" + data[i].Name + "</td><td align='center'>" + data[i].PostCode + "</td><td align='center'>" + data[i].Population + "</td><td align='center'>"+ data[i].Country.Name + "</td>";
				// prikaz dugmadi za izmenu i brisanje
				var stringId = data[i].Id.toString();
				var displayDelete = "<td><button class='btn btn-danger btn-sm' id=btnDelete1 name=" + stringId + ">Delete</button></td>";
				var displayEdit = "<td><button class='btn btn-warning btn-sm' id=btnEdit1 name=" + stringId + ">Edit</button></td>";
				row += displayData + displayDelete + displayEdit + "</tr>";  
				table.append(row);
				newId = data[i].Id;
			}
			
			div.append(table);

			// prikaz forme
			$("#formDivCity").css("display","block");

			// ispis novog sadrzaja
			$container.append(div);
		}
		else 
		{
			var div = $("<div></div>");
			var h1 = $("<h1>Greška prilikom preuzimanja Gradova!</h1>");
			div.append(h1);
			$container.append(div);
		}
	};

	function setCountry(data, status){
		
		$('#countryId').empty();
		if (status == "success") {
			console.log(data);
			// ispis drzava

			for (i = 0; i < data.length; i++) {
				$('#countryId').append('<option value="' + data[i].Id + '">' + data[i].Name + '</option>');
				console.log(data);
			}
		}
		else {
			var h1 = $("<h1>Greška prilikom preuzimanja drzava!</h1>");
		}
	};


	// dodavanje novog grada
	$("#cityForm").submit(function(e){
		// sprecavanje default akcije forme
		e.preventDefault();

		var cityName = $("#cityName").val();
		var cityPostCode = $("#cityPostCode").val();
		var cityPopulation = $("#cityPopulation").val();
		var countryId = $("#countryId option:selected").val();
		var httpAction;
		var sendData;
		var url;

		// u zavisnosti od akcije pripremam objekat
		if (formAction === "Create") {
			httpAction = "POST";
			url = host + port + cityEndpoint;
			sendData = {
				"Name": cityName,
				"PostCode": cityPostCode,
				"Population": cityPopulation,
				"CountryId": countryId
			};
		}
		else {
			httpAction = "PUT";
			url = host + port + filmsEndpoint + editingId.toString();
			sendData = {
				"Id": editingId,
				"Name": cityName,
				"PostCode": cityPostCode,
				"Population": cityPopulation,
				"CountryId": countryId
			};
		}
		

		console.log("Objekat za slanje");
		console.log(sendData);

		$.ajax({
			url: url,
			type: httpAction,
			data: sendData
		})
		.done(function(data, status){
			formAction = "Create";
			refreshTable();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		})

	});

	// brisanje autora
	function deleteCity() {
		// izvlacimo {id}
		var deleteID = this.name;
		// saljemo zahtev 
		$.ajax({
			url: host + port + cityEndpoint + deleteID.toString(),
			type: "DELETE",
		})
		.done(function(data, status){
			refreshTable();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		});

	};

	// izmena autora
	function editCity(){
		// izvlacimo id
		var editId = this.name;
		// saljemo zahtev da dobavimo tog autora
		$.ajax({
			url: host + port + cityEndpoint + editId.toString(),
			type: "GET",
		})
		.done(function(data, status){
			$("#cityName").val(data.Name);
			$("#cityPostCode").val(data.PostCode);
			$("#cityPopulation").val(data.Population);
			$("#countryId").val(data.CountryId);
			editingId = data.Id;
			formAction = "Update";
		})
		.fail(function(data, status){
			formAction = "Create";
			alert("Desila se greska!");
		});

	};

	// osvezi prikaz tabele
	function refreshTable() {
		// cistim formu
		$("#cityName").val('');
		$("#cityPostCode").val('');
		$("#cityPopulation").val('');
		$("#countryId").val('');

		
		
		// osvezavam
		$("#btnCities").trigger("click");
	};

	// osvezi pretragu

	function refreshSearch() {
		// cistim polja pretrage
		$("#minPop").val('');
		$("#maxPop").val('');

		$("#formSearch").addClass("hidden");

	}

});
