﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GradoviDrzaveService.Models
{
    public class CityCountryContext : DbContext
    {
        public CityCountryContext() : base("name=CityCountryContext") { }
        public DbSet<City> Cities { get; set; }
        public DbSet<Country> Countries { get; set; }
    }
}