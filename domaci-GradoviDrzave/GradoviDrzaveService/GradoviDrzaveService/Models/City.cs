﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GradoviDrzaveService.Models
{
    public class City
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int PostCode { get; set; }

        [Required]
        public int Population { get; set; }

        // foreign key
        public int CountryId { get; set; }
        // navigation property
        public Country Country { get; set; }
    }
}