﻿using GradoviDrzaveService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradoviDrzaveService.Interfaces
{
    public interface ICityRepository
    {
        IEnumerable<City> GetAll();
        IEnumerable<City> GetByPopulation(int min, int max);
        City GetById(int id);
        void Add(City city);
        void Update(City city);
        void Delete(City city);
    }
}
