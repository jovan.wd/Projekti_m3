﻿using GradoviDrzaveService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradoviDrzaveService.Interfaces
{
    public interface ICountryRepository
    {
        IEnumerable<Country> GetAll();
        IEnumerable<Country> GetAllWithPopulation();
        Country GetById(int id);
        void Add(Country country);
        void Update(Country country);
        void Delete(Country country);
    }
}
