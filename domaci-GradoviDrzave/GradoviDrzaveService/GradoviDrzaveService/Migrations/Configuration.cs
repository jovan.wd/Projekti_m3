namespace GradoviDrzaveService.Migrations
{
    using GradoviDrzaveService.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<GradoviDrzaveService.Models.CityCountryContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(GradoviDrzaveService.Models.CityCountryContext context)
        {
            context.Countries.AddOrUpdate(x => x.Id,
                new Country() { Id = 1, Name = "Serbia", InternationalCode = "srb" },
                new Country() { Id = 2, Name = "Deutchland", InternationalCode = "deu" },
                new Country() { Id = 3, Name = "Austria", InternationalCode = "aus" }
            );

            context.Cities.AddOrUpdate( x => x.Id,
                new City() { Id = 1, Name = "Novi Sad", PostCode = 21000, Population = 300000, CountryId = 1 },
                new City() { Id = 2, Name = "Backa Palanka", PostCode = 21400, Population = 45000, CountryId = 1 },
                new City() { Id = 3, Name = "Wienn", PostCode = 1010, Population = 1700000, CountryId = 3 },
                new City() { Id = 4, Name = "Berlin", PostCode = 10115, Population = 3500000, CountryId = 2 }
            );
        }
    }
}
