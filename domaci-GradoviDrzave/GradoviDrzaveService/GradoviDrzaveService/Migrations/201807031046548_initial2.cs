namespace GradoviDrzaveService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Countries", "Population", c => c.Int(nullable: false));
            DropColumn("dbo.Countries", "Pop");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Countries", "Pop", c => c.Int(nullable: false));
            DropColumn("dbo.Countries", "Population");
        }
    }
}
