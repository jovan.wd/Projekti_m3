﻿using GradoviDrzaveService.Interfaces;
using GradoviDrzaveService.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace GradoviDrzaveService.Repository
{
    public class CountryRepository : IDisposable, ICountryRepository
    {
        CityCountryContext db = new CityCountryContext();

        public IEnumerable<Country> GetAll()
        {
            return db.Countries;
        }
        
        public Country GetById(int id)
        {
            return db.Countries.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Country> GetAllWithPopulation()
        {
            //var list = from a in db.Countries
            //           join c in db.Cities on a.Id equals c.CountryId
            //           group new { a, c} by new { a.Id, a.Name, a.InternationalCode, c.Population} into grp
            //           select new
            //           {
            //               Count = grp.Count(),
            //               grp.Key.Population
            //           };
            //return list;
            var cities = db.Cities;
            var countries = db.Countries;

            foreach (var c in countries)
            {
                int pop = 0;
                foreach (var s in cities)
                {
                    if (c.Id == s.CountryId)
                    {
                        pop += s.Population;
                        c.Population = pop;
                    }
                }
            }

            return countries.OrderByDescending(x => x.Population);
        }

        public void Add(Country country)
        {
            db.Countries.Add(country);
            db.SaveChanges();
        }

        public void Update(Country country)
        {
            db.Entry(country).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public void Delete(Country country)
        {
            db.Countries.Remove(country);
            db.SaveChanges();
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}