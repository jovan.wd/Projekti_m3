﻿using GradoviDrzaveService.Interfaces;
using GradoviDrzaveService.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace GradoviDrzaveService.Repository
{
    public class CityRepository : IDisposable, ICityRepository
    {
        CityCountryContext db = new CityCountryContext();

        public IEnumerable<City> GetAll()
        {
            return db.Cities.Include( m=>m.Country).OrderBy(x=>x.PostCode);
        }

        public City GetById(int id)
        {
            return db.Cities.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<City> GetByPopulation(int minPop, int maxPop)
        {
            return db.Cities.Include(c => c.Country).Where(c => c.Population >= minPop).Where( c => c.Population <= maxPop);
        }

        public void Add(City city)
        {
            db.Cities.Add(city);
            db.SaveChanges();
        }

        public void Update(City city)
        {
            db.Entry(city).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public void Delete(City city)
        {
            db.Cities.Remove(city);
            db.SaveChanges();
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}