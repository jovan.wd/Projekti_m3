﻿using GradoviDrzaveService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GradoviDrzaveService.Models;

namespace GradoviDrzaveService.Controllers
{
    public class CitiesController : ApiController
    {
        ICityRepository _repository { get; set; }

        public CitiesController(ICityRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<City> Get()
        {
            return _repository.GetAll();
        }

        public IHttpActionResult Get(int id)
        {
            City city = _repository.GetById(id);

            if (city == null)
            {
                return BadRequest();
            }

            return Ok(city);
        }

        public IEnumerable<City> Get(int min, int max)
        {
            return _repository.GetByPopulation(min, max);
        }

        public IHttpActionResult Post(City city)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _repository.Add(city);
            return Ok(city);
        }

        public IHttpActionResult Put(int id, City city)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != city.Id)
            {
                return BadRequest();
            }
            try
            {
                _repository.Update(city);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(city);
        }

        public IHttpActionResult Delete(int id)
        {
            City city = _repository.GetById(id);

            if (city == null)
            {
                return BadRequest();
            }

            _repository.Delete(city);

            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}
