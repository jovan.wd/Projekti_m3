﻿using GradoviDrzaveService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GradoviDrzaveService.Models;

namespace GradoviDrzaveService.Controllers
{
    public class CountriesController : ApiController
    {
        ICountryRepository _repository { get; set; }

        public CountriesController(ICountryRepository repository)
        {
            _repository = repository;
        }
       
        public IEnumerable<Country> Get()
        {
            return _repository.GetAll();
        }


        [Route("api/statistics/")]
        public IEnumerable<Country> GetWithPopulation()
        {
            return _repository.GetAllWithPopulation();
        }

        public IHttpActionResult Get(int id)
        {
            Country country = _repository.GetById(id);

            if (country == null)
            {
                return BadRequest();
            }

            return Ok(country);
        }

        public IHttpActionResult Post(Country country)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _repository.Add(country);
            return Ok(country);
        }

        public IHttpActionResult Put(int id, Country country)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != country.Id)
            {
                return BadRequest();
            }
            try
            {
                _repository.Update(country);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(country);
        }

        public IHttpActionResult Delete(int id)
        {
            Country country = _repository.GetById(id);

            if (country == null)
            {
                return BadRequest();
            }

            _repository.Delete(country);

            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}
