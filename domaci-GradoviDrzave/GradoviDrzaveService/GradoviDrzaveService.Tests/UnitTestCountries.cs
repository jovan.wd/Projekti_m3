﻿using GradoviDrzaveService.Controllers;
using GradoviDrzaveService.Interfaces;
using GradoviDrzaveService.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;

namespace GradoviDrzaveService.Tests
{
    [TestClass]
    public class UnitTestCountries
    {
        [TestMethod]
        public void GetReturnsProductWithSameId()
        {
            // Arrange
            var mockRepository = new Mock<ICountryRepository>();
            mockRepository.Setup(x => x.GetById(42)).Returns(new Country { Id = 42 });

            var controller = new CountriesController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Get(42);
            var contentResult = actionResult as OkNegotiatedContentResult<Country>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(42, contentResult.Content.Id);
        }

        // --------------------------------------------------------------------------------------

        [TestMethod]
        public void GetReturnsNotFound()
        {
            // Arrange
            var mockRepository = new Mock<ICountryRepository>();
            var controller = new CountriesController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Get(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public void DeleteReturnsNotFound()
        {
            // Arrange 
            var mockRepository = new Mock<ICountryRepository>();
            var controller = new CountriesController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Delete(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        // --------------------------------------------------------------------------------------

        [TestMethod]
        public void DeleteReturnsOk()
        {
            // Arrange
            var mockRepository = new Mock<ICountryRepository>();
            mockRepository.Setup(x => x.GetById(10)).Returns(new Country { Id = 10 });
            var controller = new CountriesController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Delete(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        }

        // --------------------------------------------------------------------------------------

        [TestMethod]
        public void PutReturnsBadRequest()
        {
            // Arrange
            var mockRepository = new Mock<ICountryRepository>();
            var controller = new CountriesController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Put(10, new Country { Id = 9, Name = "Country1" });

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        }

        // -------------------------------------------------------------------------------------

        [TestMethod]
        public void PostMethodSetsLocationHeader()
        {
            // Arrange
            var mockRepository = new Mock<ICountryRepository>();
            var controller = new CountriesController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Post(new Country { Id = 10, Name = "Country1" });
            var createdResult = actionResult as CreatedAtRouteNegotiatedContentResult<Country>;

            // Assert
            Assert.IsNotNull(createdResult);
            Assert.AreEqual("DefaultApi", createdResult.RouteName);
            Assert.AreEqual(10, createdResult.RouteValues["id"]);
        }

        // ------------------------------------------------------------------------------------------

        [TestMethod]
        public void GetReturnsMultipleObjects()
        {
            // Arrange
            List<Country> countries = new List<Country>();
            countries.Add(new Country { Id = 1, Name = "Country1" });
            countries.Add(new Country { Id = 2, Name = "Country2" });

            var mockRepository = new Mock<ICountryRepository>();
            mockRepository.Setup(x => x.GetAll()).Returns(countries.AsEnumerable());
            var controller = new CountriesController(mockRepository.Object);

            // Act
            IEnumerable<Country> result = controller.Get();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(countries.Count, result.ToList().Count);
            Assert.AreEqual(countries.ElementAt(0), result.ElementAt(0));
            Assert.AreEqual(countries.ElementAt(1), result.ElementAt(1));
        }
    }
}
