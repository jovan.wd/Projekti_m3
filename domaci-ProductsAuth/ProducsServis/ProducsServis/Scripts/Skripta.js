﻿$(document).ready(function () {
    RefreshRegLogForm();

    // pripremanje dogadjaja za brisanje
    $("body").on("click", "#btnDelete", deleteCountry);

    // priprema dogadjaja za izmenu
    $("body").on("click", "#btnEdit", editCountry);

    // podaci od interesa
    var host = window.location.host;
    var token = null;
    var headers = {};
    var formAction = "Create";
    var editingId;
    // posto inicijalno nismo prijavljeni, sakrivamo odjavu
    $("#odjava").css("display","none");

    // registracija korisnika
    $("#registracija").submit(function (e) {
        e.preventDefault();

        var email = $("#regEmail").val();
        var loz1 = $("#regLoz").val();
        var loz2 = $("#regLoz2").val();

        // objekat koji se salje
        var sendData = {
            "Email": email,
            "Password": loz1,
            "ConfirmPassword": loz2
        };


        $.ajax({
            type: "POST",
            url: 'http://' + host + "/api/Account/Register",
            data: sendData
 
        }).done(function (data) {
            RefreshRegLogForm();
            $("#info").append("Uspešna registracija. Možete se prijaviti na sistem.");
            

        }).fail(function (data) {
            alert(data);
        });


    });


    // prijava korisnika
    $("#prijava").submit(function (e) {
        e.preventDefault();

        var email = $("#priEmail").val();
        var loz = $("#priLoz").val();

        // objekat koji se salje
        var sendData = {
            "grant_type": "password",
            "username": email,
            "password": loz
        };

        $.ajax({
            "type": "POST",
            "url": 'http://' + host + "/Token",
            "data": sendData

        }).done(function (data) {
            console.log(data);
            RefreshRegLogForm();
            $("#info").empty().append("Prijavljen korisnik: " + data.userName);
            token = data.access_token;
            $("#prijava").css("display", "none");
            $("#registracija").css("display", "none");
            $("#odjava").css("display", "block");
            

        }).fail(function (data) {
            alert(data);
        });
    });

    // odjava korisnika sa sistema
    $("#odjavise").click(function () {
        token = null;
        headers = {};
        RefreshRegLogForm();
        $("#prijava").css("display", "block");
        $("#registracija").css("display", "block");
        $("#odjava").css("display", "none");
        $("#formProduct").css("display", "none");
        $("#info").empty();
        $("#sadrzaj").empty();

    });

    $("#proizvodi").click(function () {
        // korisnik mora biti ulogovan
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        $.ajax({
            "type": "GET",
            "url": "http://" + host + "/api/products/",
            "headers": headers

        }).done(function (data, status) {
            console.log(data, status);
            var $container = $("#data");
            $container.empty();
            if (status === "success") {
                if (token !== null) {
                    // ispis naslova
                    var div = $("<div></div>");
                    var h1 = $("<h1>Products</h1>");
                    div.append(h1);
                    // ispis tabele
                    var table = $("<table border='1' class='table' style='background-color: #b3b3cc'></table>");
                    var header = $("<tr><th align='center'>Id</th><th align='center'>Name</th><th align='center'>Price</th><th align='center'>Production Date</th><th align='center'>Edit</th><th align='center'>Delete</th>");
                    table.append(header);
                    for (i = 0; i < data.length; i++) {
                        // prikazujemo novi red u tabeli
                        var row = "<tr class='hh'>";
                        // prikaz podataka
                        var displayData = "<td align='center'>" + data[i].Id + "</td><td align='center'>" + data[i].Name + "</td><td align='center'>" + data[i].Price + "</td><td align='center'>" + data[i].ProductionDate + "</td>";
                        var stringId = data[i].Id.toString();
                        var displayDelete = "<td><button class='btn btn-danger btn-sm' id=btnDelete name=" + stringId + ">Delete</button></td>";
                        var displayEdit = "<td><button class='btn btn-warning btn-sm' id=btnEdit name=" + stringId + ">Edit</button></td>";
                        row += displayData + displayDelete + displayEdit + "</tr>";
                        table.append(row);
                        newId = data[i].Id;
                    }

                    div.append(table);

                    // ispis novog sadrzaja
                    $container.append(div);
                    $("#formProduct").css("display", "block");
                }
                else
                {
                    // ispis naslova
                    div = $("<div></div>");
                    h1 = $("<h1>Products</h1>");
                    div.append(h1);
                    // ispis tabele
                     table = $("<table border='1' class='table' style='background-color: #b3b3cc'></table>");
                     header = $("<tr><th align='center'>Id</th><th align='center'>Name</th><th align='center'>Price</th><th align='center'>Production Date</th><th id='tdHidden' class='hidden' align='center'>Edit</th><th id='tdHidden' class='hidden' align='center'>Delete</th>");
                    table.append(header);
                    for (i = 0; i < data.length; i++) {
                        // prikazujemo novi red u tabeli
                         row = "<tr class='hh'>";
                        // prikaz podataka
                         displayData = "<td align='center'>" + data[i].Id + "</td><td align='center'>" + data[i].Name + "</td><td align='center'>" + data[i].Price + "</td><td align='center'>" + data[i].ProductionDate + "</td>";
                         stringId = data[i].Id.toString();
                         displayDelete = "<td id='tdHidden' class='hidden'><button class='btn btn-danger btn-sm' id=btnDelete name=" + stringId + ">Delete</button></td>";
                         displayEdit = "<td id='tdHidden' class='hidden'><button class='btn btn-warning btn-sm' id=btnEdit name=" + stringId + ">Edit</button></td>";
                        row += displayData + displayDelete + displayEdit + "</tr>";
                        table.append(row);
                        newId = data[i].Id;
                    }

                    div.append(table);

                    // ispis novog sadrzaja
                    $container.append(div);
                }
                
            }
            else
            {
                console.log(status);
                div = $("<div></div>");
                h1 = $("<h1>Greška prilikom preuzimanja drzava!</h1>");
                div.append(h1);
                $container.append(div);
            }
            

        }).fail(function (data) {
            alert(data.status + ": " + data.statusText);
        });


    });


    $("#formCreate").submit(function (e) {
        // sprecavanje default akcije forme
        e.preventDefault();

        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        var productName = $("#productName").val();
        var productPrice = $("#productPrice").val();
        var productDate = $("#productDate").val();
        var httpAction;
        var sendData;
        var url;

        // u zavisnosti od akcije pripremam objekat
        if (formAction === "Create") {
            httpAction = "POST";
            url = "http://" + host + "/api/products/";
            sendData = {
                "Name": productName,
                "Price": productPrice,
                "ProductionDate": productDate
            };
        }
        else {
            httpAction = "PUT";
            url = "http://" + host + "/api/products/" + editingId.toString();
            sendData = {
                "Id": editingId,
                "Name": productName,
                "Price": productPrice,
                "ProductionDate": productDate
            };
        }


        console.log("Objekat za slanje");
        console.log(sendData);

        $.ajax({
            url: url,
            type: httpAction,
            "headers": headers,
            data: sendData
        })
            .done(function (data, status) {
                formAction = "Create";
                refreshTable();
            })
            .fail(function (data, status) {
                alert("Desila se greska!");
            });

    });

    // brisanje drzave
    function deleteCountry() {
        
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }


        // izvlacimo {id}
        var deleteID = this.name;
        // saljemo zahtev 
        $.ajax({
            "type": "Delete",
            "url": "http://" + host + "/api/products/" + deleteID.toString(),
            "headers": headers
        })
            .done(function (data, status) {
                refreshTable();
            })
            .fail(function (data, status) {
                alert("Desila se greska!");
            });

    }

    // izmena autora
    function editCountry() {

        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }
        // izvlacimo id
        var editId = this.name;
        // saljemo zahtev da dobavimo tog autora
        $.ajax({
            "type": "Get",
            "url": "http://" + host + "/api/products/" + editId.toString(),
            "headers": headers

        })
            .done(function (data, status) {
                $("#productName").val(data.Name);
                $("#productPrice").val(data.Price);
                $("#productDate").val(data.ProductionDate);
                editingId = data.Id;
                formAction = "Update";
            })
            .fail(function (data, status) {
                formAction = "Create";
                alert("Desila se greska!");
            });

    }

    function RefreshRegLogForm() {
        $("#regEmail").val('');
        $("#regLoz").val('');
        $("#regLoz2").val('');
        $("#priEmail").val('');
        $("#priLoz").val('');

        // refresujemo tabelu ako se neko u medjuvremenu prijavi

        $("#proizvodi").trigger("click");
    }

    function refreshTable() {
        // cistim formu
        $("#productName").val('');
        $("#productPrice").val('');
        $("#productDate").val('');
        // osvezavam
        $("#proizvodi").trigger("click");
    }
    
});