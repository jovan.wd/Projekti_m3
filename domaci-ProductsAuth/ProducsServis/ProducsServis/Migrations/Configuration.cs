namespace ProducsServis.Migrations
{
    using ProducsServis.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ProducsServis.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ProducsServis.Models.ApplicationDbContext context)
        {
            context.Products.AddOrUpdate(x => x.Id,
                new Product() { Id = 1, Name = "Shoes", Price = 10.99m, ProductionDate = new DateTime(2018,4,5,6,13,23) },
                new Product() { Id = 2, Name = "Jeans", Price = 15.50m, ProductionDate = new DateTime(2016, 10, 11, 8, 0, 0) },
                new Product() { Id = 3, Name = "Jacket", Price = 29.99m, ProductionDate = new DateTime(2016, 6, 5, 16, 15, 12) },
                new Product() { Id = 4, Name = "T-Shirt", Price = 5.50m, ProductionDate = new DateTime(2010, 12, 12, 20, 01, 59) },
                new Product() { Id = 5, Name = "Hat", Price = 2.8m, ProductionDate = new DateTime(2000, 4, 4, 8, 30, 0) }
                );
        }
    }
}
